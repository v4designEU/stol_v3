# STOL_v3

Spatio-Temporal Object Localization code of the V4Design version 3 for images and videos.

This repository contains an "input.json" file as an input example, in order to run the codes. If the frames in "input.json" contain outdoor scenes "STOL_v3_new_msg_err_mng.py" will give you an empty "output_STOL.json". Otherwise, if the "input.json" has frames that contain indoor scenes, the execution of "STOL_v3_new_msg_err_mng.py" will return an "output_STOL.json" with the results.

**Information on the libraries used:**

The code is written in Python 3.7 and run in Anaconda3 environment. You can create an Anaconda3 environment and use commands given below to install the libraries used:

* Tensorflow-gpu == 1.14.0 (conda install -c anaconda tensorflow-gpu==1.14.0)
* OpenCV == 4.2.0 (conda install -c anaconda opencv==4.2.0)
* Keras-gpu == 2.2.4 (conda install -c anaconda keras-gpu==2.2.4)
* Pandas == 0.25.3 (conda install -c anaconda pandas==0.25.3)
* Natsort == 6.2.0 (conda install -c anaconda natsort==6.2.0)
* PIL == 6.2.1 (conda install -c anaconda pillow==6.2.1) 
* Coco (install cocoapi, install pycocotools)
* Mrcnn (pip install mrcnn)
* imgaug (pip install imgaug)

**To install the component:**

1. download or clone this repository
2. download the updated v3 model files from https://drive.google.com/file/d/1PkJxZRoHcxXvAQRoIHNU8eefYUEQE-0M/view?usp=sharing or contact: klearchos_stav@iti.gr or tpistola@iti.gr
3. change paths in the scripts (change "my_dir")
4. execute "STOL_v3_new_msg_err_mng.py" script for object localization

**Note:** Credentials are required in order to run this service, please request a username and pass via email (klearchos_stav@iti.gr and tpistola@iti.gr)
