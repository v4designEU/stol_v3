"""
Created on October 30th, 2019
@author: Theodora Pistola

Updated on January 23rd, 2020 (add pickle file to keep STOL shots' history)
@author: Theodora Pistola

Updated on May 11, 2020 (v3 bus messages)
@author: Theodora Pistola

Updated on June 22, 2020 (v3 error management)
@author: Klearchos Stavrothanasopoulos

Updated on July 20, 2020 (v3 KB and bus messages)
@author: Klearchos Stavrothanasopoulos
"""

# necessary imports
import numpy as np
import pandas
import time
import os, sys
import os.path
from os import path
import csv
import json
import re
import cv2
import shutil
from web_functions import WebFunctions  # .send_post\
from coco import coco
import tensorflow as tf
from keras import backend as k
# Import Mask RCNN
from mrcnn import utils
import mrcnn.model as modellib
import natsort
from datetime import datetime
import pickle
import pytz   # for timezones


# ------------------------------------------------------------ #
# ------------- FUNCTIONS AND CLASSES NEEDED ----------------- #
# ------------------------------------------------------------ #


# ---------------- for Mask RCNN ---------------- #
def random_colors(N):
    np.random.seed(1)
    colors = [tuple(255 * np.random.rand(3)) for _ in range(N)]
    return colors


def apply_mask(image, mask, color, alpha=0.5):
    """apply mask to image"""
    for n, c in enumerate(color):
        image[:, :, n] = np.where(
            mask == 1,
            image[:, :, n] * (1 - alpha) + alpha * c,
            image[:, :, n]
        )
    return image


def display_instances(image, boxes, masks, ids, names, scores):
    """
        take the image and results and apply the mask, box, and Label
    """
    n_instances = boxes.shape[0]
    colors = random_colors(n_instances)

    if not n_instances:
        print('NO INSTANCES TO DISPLAY')
    else:
        assert boxes.shape[0] == masks.shape[-1] == ids.shape[0]

    for i, color in enumerate(colors):
        if not np.any(boxes[i]):
            continue

        y1, x1, y2, x2 = boxes[i]
        label = names[ids[i]]
        score = scores[i] if scores is not None else None
        caption = '{} {:.2f}'.format(label, score) if score else label
        mask = masks[:, :, i]

        image = apply_mask(image, mask, color)
        image = cv2.rectangle(image, (x1, y1), (x2, y2), color, 2)
        image = cv2.putText(
            image, caption, (x1, y1), cv2.FONT_HERSHEY_COMPLEX, 0.7, color, 2
        )

    return image


def get_only_mask(image, boxes, masks, ids, names, scores):
    """
        give as output the extracted mask only as image
    """
    n_instances = boxes.shape[0]
    colors = random_colors(n_instances)

    if not n_instances:
        print('NO INSTANCES TO DISPLAY')
    else:
        assert boxes.shape[0] == masks.shape[-1] == ids.shape[0]

    # create empty mask
    h, w, c = image.shape
    mask = np.zeros((h, w))
    for i, color in enumerate(colors):
        if not np.any(boxes[i]):
            continue

        # y1, x1, y2, x2 = boxes[i]
        # label = names[ids[i]]
        # score = scores[i] if scores is not None else None
        # caption = '{} {:.2f}'.format(label, score) if score else label
        current_mask = masks[:, :, i]
        current_mask = current_mask.astype(np.uint8)
        current_mask *= (ids[i])

        # in order to keep one of the overlapped objects (so that we can decode the grayscale mask to a binary for the desired object)
        for m in range(0, h):
            for n in range(0, w):
                if not current_mask[m, n] == 0 and not mask[m, n] == 0:
                    mask[m, n] = 0

        mask = mask + current_mask

        # Done: Extract one mask for all objects found in image. Give different value to every mask. Grayscale image as output mask!

    return mask


class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    NUM_CLASSES = 101
# define flag for Error Management <------- 0 no error, 1 error found
found_error = 0

#try:

######################################################################
# TensorFlow wizardry
config = tf.ConfigProto()

# Don't pre-allocate memory; allocate as-needed
config.gpu_options.allow_growth = True

# Only allow a total of half the GPU memory to be allocated
config.gpu_options.per_process_gpu_memory_fraction = 0.5

# Create a session with the above options specified.
k.tensorflow_backend.set_session(tf.Session(config=config))

url_retrieve = 'http://160.40.49.184:10010/retrieve'
url_save = 'http://160.40.49.184:10010/save/OL'
######################################################################

#### define directory
my_dir = 'D:/RunningServices/V4Design/V4D_Visual_Analysis/STBOL_new/'  # enter the dir name  # #"D:/tpistola/V4Design/Codes/Dora_Services/STBOL_new/"     # 'D:/RunningServices/V4Design/V4D_Visual_Analysis/STBOL_new/'

#### delete video files
for fname in os.listdir(my_dir):
    if fname.startswith("STOL_videos_"):
        print((os.path.join(my_dir, fname)))
        shutil.rmtree(os.path.join(my_dir, fname), ignore_errors=True)

#### delete image files
for imname in os.listdir(my_dir):
    if imname.startswith("STOL_images_"):
        shutil.rmtree(os.path.join(my_dir, imname), ignore_errors=True)

#### import input json file
with open('input_ol.json') as data_file:
    full_bus = json.loads(data_file.read())
    header = full_bus["header"]
    timestamp = header["timestamp"]
    body = full_bus["body"]
    info = body["data"]
    # info = body["info"]

# if not type(full_bus) == dict:
if len(info) > 1:
    bus = info[0]  # video
else:
    bus = info  # image

############################################################################

MODEL_DIR = my_dir + "MaskRCNN_models/"
TRAINED_MODEL = MODEL_DIR + "mask_rcnn_coco_ade_lvis_0160.h5"
"""
class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
               'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']
"""
#also change num_classes in coco file, COCO = 81 , ADE_LVIS = 101

class_names = ['BG', 'bed', 'windowpane', 'cabinet', 'person', 'door',
               'table', 'curtain', 'chair', 'car', 'painting', 'sofa',
               'shelf', 'mirror', 'armchair', 'seat', 'fence', 'desk', 'wardrobe',
               'lamp', 'bathtub', 'railing', 'cushion','box', 'column', 'signboard',
               'chest of drawers', 'counter', 'sink',
               'fireplace', 'refrigerator', 'stairs', 'case', 'pool table',
               'pillow', 'screen door', 'bookcase', 'coffee table',
               'toilet', 'flower', 'book', 'bench', 'countertop',
               'stove', 'palm', 'kitchen island', 'computer', 'swivel chair', 'boat',
               'arcade machine', 'bus', 'towel', 'light', 'truck', 'chandelier',
               'awning', 'streetlight', 'booth', 'television receiver', 'airplane', 'apparel',
               'pole', 'bannister', 'ottoman', 'bottle', 'van', 'ship',
               'fountain', 'washer', 'plaything', 'stool', 'barrel',
               'basket', 'bag', 'minibike', 'oven', 'ball', 'food',
               'step', 'trade name', 'microwave',
               'pot', 'animal', 'bicycle', 'dishwasher', 'screen', 'sculpture',
               'hood', 'sconce', 'vase','traffic light', 'tray', 'ashcan', 'fan', 'plate', 'monitor',
               'bulletin board', 'radiator', 'glass', 'clock', 'flag']
###########################################################################

# ------------------------------------------------------------ #
# ------------------------ FOR WEBPAGES ---------------------- #
# ------------------------------------------------------------ #

# print()
#
# if bus['entity'] == "webpage":
#     print("[INFO] Webpage")
#     # ToDo: insert code!!!!
#
# else:
#     if (not bus['entity'] == "image") and (not bus['entity'] == "video"):
#         print(['[INFO] Do nothing'])
#
#         # ToDo: insert code!!!!

# ------------------------------------------------------------ #
# --------------- FOR IMAGE COLLECTION ----------------------- #
# ------------------------------------------------------------ #

if bus['entity'] == "image_collection":
    print('[INFO] Image Collection')

    frame_ids = []
    for b in info:
        # print(b)  # prints info for each frame
        if b['outdoor'] == False:
            frame_ids.append(b['image'] + '.jpg')

    if len(frame_ids):
        print('[INFO] Indoor Scene')

        body = {"task": "CR", "entity": bus['entity'], "field": "id", "value": bus['collectionId']}
        print(("collectionId: ", body["value"]))

        # count the whole time for image segmentation
        start_image = time.time()

        # create new paths
        os.chdir(my_dir)
        path = my_dir + "STOL_images_" + str(body["value"] + "/")
        path2 = my_dir + "STOL_images_" + str(body["value"] + '_2/')
        path_mask_im = my_dir + "STOL_images_" + str(body["value"] + '_masks/')
        os.mkdir(path)
        os.mkdir(path2)
        os.mkdir(path_mask_im)

        z = 0  # index for the images inside the collection loop below

        # download metadata

        json_metadata = WebFunctions.send_post(url_retrieve, body)
        json_meta = json.loads(json_metadata)
        objects = json_meta["objects"][1]
        # define image url

        imgs_urls = []
        count_outd_img = 0

        for object1 in objects:
            a = object1[1]
            if 'alternativeUrl' in a:
                imgs_urls.append(a['alternativeUrl'])
            else:
                imgs_urls.append(a['webPageUrl'])
        """
        for frame in frame_ids:
            url_frame = "http://160.40.49.184:10010/retrieve-mm?entity=frame&simmo=" + body["value"] + "&idx=" + str(frame)
            print(url_frame)
            try:
                frame_download = WebFunctions.download_from_url(url_frame, path + '/' + url_frame.split("=")[-1] + ".jpg")
            except:
                print(
                    "-----> Error Management Message - Reason: Unable to download frame from KUL.")  # Create error management json to send to bus
                error_msg = {}
                error_msg["header"] = {}
                error_msg["header"]["sender"] = "STBL"
                error_msg["header"]["timestamp"] = str(datetime.now(tz=pytz.timezone('CET'))) + " CET"  # CET time
        """    
        for img_url in imgs_urls:  # processing each image

            image_download = WebFunctions.download_from_url(img_url, path + img_url.split("/")[-1])


        url = imgs_urls[0]

        images = os.listdir(path)
        images = natsort.natsorted(images)

        frame_ids = natsort.natsorted(frame_ids)
        print(frame_ids)

        for img1 in frame_ids:
            img_path = path + img1
            # print(img_path)
            # load image
            img = cv2.imread(img_path)

            # check if image size is very big
            height, width, channels = img.shape
            if (height > 1024 or width > 1024):  # TODO: Is this a good threshold?
                img = cv2.resize(img, (0, 0), fx=0.3, fy=0.3)
                cv2.imwrite(os.path.join(path, img1), img)

            img = cv2.imread(img_path)
            height, width, channels = img.shape
            if (height > 1024 or width > 1024):  # TODO: Is this a good threshold?
                img = cv2.resize(img, (0, 0), fx=0.3, fy=0.3)
                cv2.imwrite(os.path.join(path, img1), img)

                # For Mask RCNN
        config = InferenceConfig()
        config.display()

        # Create model object in inference mode.
        model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

        # Load weights trained on MS-COCO
        model.load_weights(TRAINED_MODEL, by_name=True)
        print("Mask R-CNN model is loaded!")

        # Start creating output for bus
        dict_info4 = {}
        dict_info3 = {}
        dict_info3["timestamp"] = str(datetime.now(tz=None))
        dict_info3["sender"] = "STOL"
        dict_info4["header"] = dict_info3
        # dict_info4["body"] = []
        dict_info4["body"] = {}
        dict1 = {}
        dict1["data"] = []

        counter = 0

        for image2 in frame_ids:  # processing each img of the collection

            # download image
            # image_download = WebFunctions.download_from_url(url, path + url.split("/")[-1])

            print("image: ", image2)

            img_path = path + image2

            # load image
            img = cv2.imread(img_path)

            # check if image size is very big
            h, w, c = img.shape

            """
            # create dict_info dictionary to save our results
            # out_info = []  # list of dictionaries
            dict_info = {}
            dict_info["colllectionId"] = []
            dict_info["mask_url"] = []
            dict_info["cmp_mask_url"] = []
            dict_info["final_mask_url"] = []
            dict_info["tags"] = []
            """

            # create dict_info dictionary to save our results for bus
            # out_info = []  # list of dictionaries
            dict_info2 = {}
            dict_info2["collection_id"] = []
            dict_info2["media_id"] = []
            dict_info2["image"] = []
            dict_info2["final_mask_url"] = []
            dict_info2["tags"] = []
            dict_info2["shot_idx"] = []
            dict_info2["user_style"] = []

            """
            # check if image size is very big
            height, width, channels = img.shape
            if (height > 1024 or width > 1024):
                img = cv2.resize(img, (0, 0), fx=0.5, fy=0.5)
            """

            # Run detection
            results = model.detect([img], verbose=1)

            # Visualize results
            r = results[0]

            # create dictionary to keep existing tags extracted from segmentation
            existing_tags_dict2 = {}
            existing_tags_dict2["tags"] = []
            existing_tags_list = []
            existing_tags_list_kb = []

            cur_tag_dict = {}
            cur_tag_dict_kb = {}

            print("This image/frame contains:")

            for t in range(0, len(r[
                                      'class_ids'])):  # Check if there are > 1 instances of the same tag in order to have one list of bboxes!
                id = r['class_ids'][t]
                if id > 0:

                    # simply put every instance and its info as a tag
                    print(("{} with probability {:.2f}%".format(class_names[id], 100 * r['scores'][t])))
                    cur_tag_dict = {}
                    bbox_list = []
                    cur_tag_dict["type"] = class_names[id]
                    cur_tag_dict["probability"] = str(r['scores'][t])

                    bbox_dict = {}
                    bbox_dict['x'] = str(r['rois'][t][0])
                    bbox_dict['y'] = str(r['rois'][t][1])
                    bbox_dict['height'] = str(r['rois'][t][2])
                    bbox_dict['width'] = str(r['rois'][t][3])

                    bbox_list.append(bbox_dict)
                    cur_tag_dict["bbox_list"] = bbox_list

                    existing_tags_list.append(cur_tag_dict)

                    if image2 == frame_ids[0]:
                        cur_tag_dict_kb = {}
                        cur_tag_dict_kb["frame"] = ""
                        cur_tag_dict_kb["type"] = class_names[id]
                        cur_tag_dict_kb["probability"] = float(r['scores'][t])
                        # bbox_list = []  # TODO: extract bboxes????
                        # cur_tag_dict["bbox_list"] = bbox_list
                        existing_tags_list_kb.append(cur_tag_dict_kb)

            if not len(r['class_ids']) == 0:
                seg_image = display_instances(img, r['rois'], r['masks'], r['class_ids'],
                                              class_names, r['scores'])

                mask_image = get_only_mask(img, r['rois'], r['masks'], r['class_ids'],
                                           class_names, r['scores'])
                # save segmented image & mask
                if not os.path.exists(path_mask_im):
                    os.makedirs(path_mask_im)
                cv2.imwrite(path_mask_im + os.path.splitext(image2)[0] + '_mask_plus.png', seg_image)
                cv2.imwrite(path_mask_im + os.path.splitext(image2)[0] + '_mask.png', mask_image)

                # request url for mapillary mask
                metavliti = WebFunctions.encode_file(
                    path_mask_im + os.path.splitext(image2)[0] + "_mask.png")  # encode file to String variable
                uploaded_file_url = WebFunctions.send_post('http://160.40.49.184:10010/upload/test', metavliti,
                                                           False)
                print(uploaded_file_url)  # print the url where the file was uploaded
                g = uploaded_file_url
            else:
                h, w, c = img.shape
                mask_image = np.zeros((h, w))

                # save segmented image & mask
                if not os.path.exists(path_mask_im):
                    os.makedirs(path_mask_im)
                cv2.imwrite(path_mask_im + os.path.splitext(image2)[0] + '_mask.png', mask_image)

                # request url for mapillary mask
                metavliti = WebFunctions.encode_file(path_mask_im + os.path.splitext(image2)[
                    0] + "_mask.png")  # encode file to String variable
                uploaded_file_url = WebFunctions.send_post('http://160.40.49.184:10010/upload/test', metavliti,
                                                           False)
                print(uploaded_file_url)  # print the url where the file was uploaded
                g = uploaded_file_url

            b = body["value"]  # simmo id

            # save existing tags in t in order to write them in output json
            t = existing_tags_list

            # Save to datastorage
            # print(dict_info)
            if image2 == frame_ids[0]:
                dict_kb = {}
                dict_kb["simmo"] = b
                dict_kb["shotIdx"] = bus["shot_idx"]
                # dict_kb["first_frame"] = frame_ids[0]
                # dict_kb["last_frame"] = frame_ids[-1]
                dict_kb["final_mask_url"] = g
                dict_kb["tags"] = existing_tags_list_kb
                json_to_datastorage = WebFunctions.send_post(url_save, dict_kb)
                with open('output_KB_STOL.json', 'w') as kle:
                    json.dump(dict_kb, kle)

            # save current frame's info in dict_info and the in out_info list
            dict_info2["simmo_id"] = b
            dict_info2["media_id"] = json_to_datastorage
            dict_info2["user_style"] = bus["user_style"]
            dict_info2["shot_idx"] = bus["shot_idx"]
            dict_info2["image"] = os.path.splitext(image2)[0]
            dict_info2["final_mask_url"] = g
            dict_info2["tags"] = t
            # dict_info4["body"].append(dict_info2)
            dict1["data"].append(dict_info2)
            dict_info4["body"] = dict1

        # write json file for bus
        with open('output_STOL.json', 'w') as outfile:
            json.dump(dict_info4, outfile)

        end_image = time.time() - start_image
        print(("Image segmentation took {} sec".format(end_image)))

    else:
        # create an empty output file
        open('output_STOL.json', 'w')


# ------------------------------------------------------------ #
# ------------------------- FOR VIDEOS ----------------------- #
# ------------------------------------------------------------ #

if bus['entity'] == "video":
    print('[INFO] Video')

    # count the whole video processing time
    time_video = time.time()

    body = {"task": "CR", "entity": bus['entity'], "field": "id", "value": bus['simmo_id']}
    print(("Simmo-id: ", body["value"]))

    #input_dict = {"simmo_id": body["value"], "shotIdx": bus["shotIdx"]}

    # Write log file
    file_txt = open("STOL_log.txt", "a")
    file_txt.write("*** Input: {}   Simmo: {}, shot_idx: {}".format(str(datetime.now(tz=None)), body["value"], bus["shot_idx"]))

    # write csv log file with simmo_id and shoIdx
    flag_process = 1
    log_history_file = "STOL_history_pickle_v3"

    out_info2 = {}
    if path.exists(log_history_file):
        ######## read pickle file ########
        # for reading also binary mode is important
        dbfile = open(log_history_file, 'rb')
        db = pickle.load(dbfile)
        print("DB: ", db)
        print("LENGTH OF DB: ", len(db))
        for idx in range(0, len(db)):
            #print("idx = ", idx)
            db_body = db[idx]["body"]
            db_data = db_body["data"]
            if not db[idx] == {}:
                #print("db_data[0] = ", db_data[0])
                if "simmo_id" in db_data[0]:
                    print("has simmo key!")
                    if body["value"] == db_data[0]["simmo_id"] and bus["shot_idx"] == db_data[0]["shot_idx"] and bus["user_style"] == db_data[0]["user_style"]:
                        print("[INFO] Info for this shot is saved in KB!")
                        flag_process = 0
                        dict_info4 = db[idx]
        dbfile.close()
        ##################################
    else:
        db = []

    if flag_process == 1:
        # from input json get only outdoor frames   TODO !!!!
        # save in frame_ids list
        frame_ids = []
        for b in info:
            # print(b)  # prints info for each frame
            if b['outdoor'] == False:
                frame_ids.append(b['frame'])

        print(frame_ids)

        # check if SR frames come as 1/8 or 1/1
        if len(frame_ids) <= 1:  # if SR sent info only for a single frame
            check_per = 1
        else:
            check_per = int(frame_ids[1]) - int(frame_ids[0])

        print("[INFO] Input frames interval: ", check_per)

        # check if there is at least 1 indoor frame
        if len(frame_ids) > 1:

            # create new paths for new video
            path1 = my_dir + "STOL_videos_" + str(body["value"]) + '_1/'
            path2 = my_dir + "STOL_videos_" + str(body["value"]) + '_2/'
            path3 = my_dir + "STOL_videos_" + str(body["value"]) + '_3/'
            path_mask_im = my_dir + "STOL_videos_" + str(body["value"]) + '_masks/'
            os.mkdir(path1)
            os.mkdir(path2)
            os.mkdir(path3)
            os.mkdir(path_mask_im)

            # download metadata
            try:
                json_metadata = WebFunctions.send_post(url_retrieve, body)
                json_meta = json.loads(json_metadata)

                # define url
                if 'alternativeUrl' in json_meta:
                    url = json_meta['alternativeUrl']
                elif 'url' in json_meta:
                    url = json_meta['url']
                else:
                    url = json_meta['webPageUrl']
                    url = 'https:' + url
            except:
                print(
                    "-----> Error Management Message - Reason: Unable to get video url.")  # Create error management json to send to bus
                error_msg = {}
                error_msg["header"] = {}
                error_msg["header"]["sender"] = "STOL"
                error_msg["header"]["timestamp"] = str(datetime.now(tz=pytz.timezone('CET'))) + " CET"  # CET time
                er_body = {}
                er_body["reason"] = "N/A"
                er_body["data"] = []
                data2 = {}
                data2["simmo_id"] = body["value"]
                data2["shot_idx"] = bus["shot_idx"]
                data2["user_style"] = bus["user_style"]
                er_body["data"].append(data2)
                data2["URL"] = "N/A"
                er_body["message_for"] = []
                er_body["message_for"].append("KB")
                error_msg["body"] = er_body
                with open("output_STOL.json", "w") as json_file:
                    json.dump(error_msg, json_file)

                found_error = 1
                sys.exit()

            for frame in frame_ids:
                url_frame = "http://160.40.49.184:10010/retrieve-mm?entity=frame&simmo=" + body[
                    "value"] + "&idx=" + str(frame)
                try:
                    frame_download = WebFunctions.download_from_url(url_frame,
                                                                    path1 + '/' + url_frame.split("=")[-1] + ".jpg")
                except:
                    print(
                        "-----> Error Management Message - Reason: Unable to download frame from KUL.")  # Create error management json to send to bus
                    error_msg = {}
                    error_msg["header"] = {}
                    error_msg["header"]["sender"] = "STOL"
                    error_msg["header"]["timestamp"] = str(
                        datetime.now(tz=pytz.timezone('CET'))) + " CET"  # CET time
                    er_body = {}
                    er_body["reason"] = "Unable to download frame from KUL"
                    er_body["data"] = []
                    data2 = {}
                    data2["simmo_id"] = body["value"]
                    data2["shot_idx"] = bus["shot_idx"]
                    data2["user_style"] = bus["user_style"]
                    er_body["data"].append(data2)
                    data2["URL"] = "N/A"
                    er_body["message_for"] = []
                    er_body["message_for"].append("KB")
                    error_msg["body"] = er_body
                    with open("output_STOL.json", "w") as json_file:
                        json.dump(error_msg, json_file)

                    found_error = 1
                    sys.exit()

                img = cv2.imread(path1 + '/' + url_frame.split("=")[-1] + ".jpg")
                img = cv2.resize(img, (0, 0), fx=0.3, fy=0.3)

                # get dimensions of image
                h, w, c = img.shape

                # save frame as JPEG file
                cv2.imwrite(os.path.join(path3, url_frame.split("=")[-1] + ".jpg"), img)

            """    
            # download video
            try:
                print("video_download")
                video_download = WebFunctions.download_from_url(url, path1 + url.split("/")[-1])
            except:
                print(
                    "-----> Error Management Message - Reason: Unable to download video.")  # Create error management json to send to bus
                error_msg = {}
                error_msg["header"] = {}
                error_msg["header"]["sender"] = "STOL"
                error_msg["header"]["timestamp"] = str(datetime.now(tz=pytz.timezone('CET'))) + " CET"  # CET time
                er_body = {}
                er_body["reason"] = "N/A"
                er_body["data"] = []
                data2 = {}
                data2["simmo_id"] = body["value"]
                data2["shot_idx"] = bus["shot_idx"]
                data2["user_style"] = bus["user_style"]
                er_body["data"].append(data2)
                data2["URL"] = url
                er_body["message_for"] = []
                er_body["message_for"].append("KB")
                error_msg["body"] = er_body
                with open("output_STOL.json", "w") as json_file:
                    json.dump(error_msg, json_file)

                found_error = 1
                sys.exit()

            videos = os.listdir(path1)
            print(videos)
            vd_path = path1 + (videos[0])
            print(vd_path)

            # load video
            vidcap = cv2.VideoCapture(vd_path)
            success, imag = vidcap.read()
            count = 0  # keeps number of frames

            # temporally save video frames
            while success:
                # resize frame
                imag = cv2.resize(imag, (0, 0), fx=0.5, fy=0.5)

                # save frame as JPEG file
                cv2.imwrite(
                    os.path.join(path3, videos[0].split("/")[-1].split(".")[0] + '_' + "{:d}.jpg".format(count)),
                    imag)

                success, imag = vidcap.read()
                count += 1
            """
            file = os.listdir(path3)

            # create out_info list to save our results
            out_info = []  # list of dictionaries

            # create out_info list to save our results for bus json
            out_info2 = []  # list of dictionaries

            # For Mask RCNN
            config = InferenceConfig()
            config.display()

            # Create model object in inference mode.
            model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

            # Load weights trained on MS-COCO
            model.load_weights(TRAINED_MODEL, by_name=True)

            print("Mask R-CNN model is loaded!")

            # open csv file to save segmentation info
            start = time.time()

            # Start creating output for bus
            dict_info4 = {}
            dict_info3 = {}
            dict_info3["timestamp"] = str(datetime.now(tz=None))
            dict_info3["sender"] = "STOL"
            dict_info4["header"] = dict_info3
            # dict_info4["body"] = []
            dict_info4["body"] = {}
            dict1 = {}
            dict1["data"] = []

            # Perform inference.
            # if check_per == 8:  # if the input from SR gives frames 1/8
            #     per = 1  # STOL analyzes 1/8 frames
            # else:  # if the input from SR gives frames 1/1
            #     per = 8
            per = 8

            # for frame in frame_ids:
            end_frame = int(frame_ids[len(frame_ids)-1])
            for frame in range(int(frame_ids[0]), end_frame + per, per):

                print("Frame: ", frame)

                images = os.listdir(path3)
                images = natsort.natsorted(images)  # put in numerical order

                content_img_path = (path3 + '/' + str(frame) + '.jpg')
                print(content_img_path)

                """
                dict_info = {}
                dict_info["simmo"] = []
                dict_info["frame"] = []
                dict_info["mask_url"] = []
                dict_info["cmp_mask_url"] = []
                dict_info["final_mask_url"] = []
                dict_info["tags"] = []
                """

                dict_info2 = {}
                dict_info2["simmo_id"] = []
                dict_info2["media_id"] = []
                dict_info2["frame"] = []
                dict_info2["final_mask_url"] = []
                dict_info2["tags"] = []
                dict_info2["shot_idx"] = []
                dict_info2["user_style"] = []

                # load image
                img = cv2.imread(content_img_path)

                # Run detection
                results = model.detect([img], verbose=1)

                # Visualize results
                r = results[0]

                # create dictionary to keep existing tags extracted from segmentation
                existing_tags_dict2 = {}
                existing_tags_dict2["tags"] = []
                existing_tags_list = []
                existing_tags_list_kb = []
                cur_tag_dict = {}
                cur_tag_dict_kb = {}
                flag = 0
                print("This image/frame contains:")
                for t in range(0, len(r['class_ids'])):
                    id = r['class_ids'][t]

                    if id > 0:
                        # simply put every instance and its info as a tag
                        print(("{} with probability {:.2f}%".format(class_names[id], 100 * r['scores'][t])))
                        cur_tag_dict = {}
                        bbox_list = []
                        cur_tag_dict["type"] = class_names[id]
                        cur_tag_dict["probability"] = float(r['scores'][t])

                        bbox_dict = {}
                        bbox_dict['x'] = str(r['rois'][t][0])
                        bbox_dict['y'] = str(r['rois'][t][1])
                        bbox_dict['height'] = str(r['rois'][t][2])
                        bbox_dict['width'] = str(r['rois'][t][3])

                        bbox_list.append(bbox_dict)
                        cur_tag_dict["bbox_list"] = bbox_list

                        existing_tags_list.append(cur_tag_dict)

                        if frame == int(frame_ids[0]):
                            cur_tag_dict_kb = {}
                            cur_tag_dict_kb["frame"] = ""
                            cur_tag_dict_kb["type"] = class_names[id]
                            cur_tag_dict_kb["probability"] = float(r['scores'][t])
                            #bbox_list = []  # TODO: extract bboxes????
                            #cur_tag_dict["bbox_list"] = bbox_list
                            existing_tags_list_kb.append(cur_tag_dict_kb)

                if not len(r['class_ids']) == 0:

                    # visualize results
                    seg_image = display_instances(img, r['rois'], r['masks'], r['class_ids'],
                                                  class_names, r['scores'])

                    mask_image = get_only_mask(img, r['rois'], r['masks'], r['class_ids'],
                                               class_names, r['scores'])

                    # save segmented image & mask
                    if not os.path.exists(path_mask_im):
                        os.makedirs(path_mask_im)
                    cv2.imwrite(path_mask_im + str(frame) + '_mask_plus.png', seg_image)
                    cv2.imwrite(path_mask_im + str(frame) + '_mask.png', mask_image)

                    # request url for mapillary mask
                    metavliti = WebFunctions.encode_file(path_mask_im + str(frame) + "_mask.png")  # encode file to String variable
                    uploaded_file_url = WebFunctions.send_post('http://160.40.49.184:10010/upload/test', metavliti,
                                                               False)
                    print(uploaded_file_url)  # print the url where the file was uploaded
                    g = uploaded_file_url
                else:
                    h, w, c = img.shape
                    mask_image = np.zeros((h, w))

                    # save segmented image & mask
                    if not os.path.exists(path_mask_im):
                        os.makedirs(path_mask_im)
                    cv2.imwrite(path_mask_im + str(frame) + '_mask.png', mask_image)

                    # request url for mapillary mask
                    metavliti = WebFunctions.encode_file(path_mask_im + str(frame) + "_mask.png")  # encode file to String variable
                    uploaded_file_url = WebFunctions.send_post('http://160.40.49.184:10010/upload/test', metavliti,
                                                               False)
                    print(uploaded_file_url)  # print the url where the file was uploaded
                    g = uploaded_file_url

                b = body["value"]  # simmo id

                # save existing tags in t in order to write them in output json
                t = existing_tags_list

                # save current frame's info in dict_info and the in out_info list
                #dict_info["simmo"] = b
                #dict_info["frame"] = str(frame)
                #dict_info["final_mask_url"] = g
                #dict_info["tags"] = t

                # Save to datastorage
                #print(dict_info)
                if frame == int(frame_ids[0]):
                    dict_kb = {}
                    dict_kb["simmo"] = b
                    dict_kb["shotIdx"] = bus["shot_idx"]
                    dict_kb["first_frame"] = frame_ids[0]
                    dict_kb["last_frame"] = frame_ids[-1]
                    dict_kb["final_mask_url"] = g
                    dict_kb["tags"] = existing_tags_list_kb
                    json_to_datastorage = WebFunctions.send_post(url_save, dict_kb)
                    with open('output_KB_test_STOL.json', 'w') as kle:
                            json.dump(dict_kb, kle)


                # save current frame's info in dict_info and the in out_info list
                dict_info2["simmo_id"] = b
                dict_info2["media_id"] = json_to_datastorage
                dict_info2["user_style"] = bus["user_style"]
                dict_info2["shot_idx"] = bus["shot_idx"]
                dict_info2["frame"] = str(frame)
                dict_info2["final_mask_url"] = g
                dict_info2["tags"] = t
                # dict_info4["body"].append(dict_info2)
                dict1["data"].append(dict_info2)
                dict_info4["body"] = dict1

                #out_info.append(dict_info)
                # out_info2.append(dict_info2)

            # write json file for KB
            #with open(path2 + 'my_ouput_STOL.json', 'w') as outfile:
                #json.dump(out_info, outfile)

            # write json file for bus
            with open('output_STOL.json', 'w') as outfile:
                json.dump(dict_info4, outfile)

            full_time_end = time.time() - time_video
            print("Time needed : {} sec".format(full_time_end))

            # input_dict["output"] = dict_info3
            db.append(dict_info4)

            print("[INFO] Appended DB: ", db)

            ########## write info in pickle file ##########
            dbfile = open(log_history_file, 'wb')

            # source, destination
            pickle.dump(db, dbfile)
            dbfile.close()
            ################################################

        # there is no indoor frame
        else:
            # create empty output file for bus
            open('output_STOL.json', 'w')
            dict_info4 = {}
            print("No indoor frames found. Wrote empty output_STOL.json")


    # write json file for bus
    with open('output_STOL.json', 'w') as outfile:
        json.dump(dict_info4, outfile)

    file_txt.write("\n*** Output: {}\n\n".format(str(datetime.now(tz=None))))
    file_txt.close()
# except:
#     if found_error == 0:
#         print("-----> Error Management Message - Reason: N/A<-----")
#
#         # Create error management json to send to bus
#         error_msg = {}
#         error_msg["header"] = {}
#         error_msg["header"]["sender"] = "STOL"
#         error_msg["header"]["timestamp"] = str(datetime.now(tz=pytz.timezone('CET'))) + " CET"  # CET time
#         er_body = {}
#         er_body["reason"] = "N/A"
#         er_body["data"] = []
#         data2 = {}
#         data2["simmo_id"] = bus["simmo_id"]
#         data2["shot_idx"] = bus["shot_idx"]
#         data2["user_style"] = bus["user_style"]
#         er_body["data"].append(data2)
#         er_body["message_for"] = []
#         er_body["message_for"].append("KB")
#         error_msg["body"] = er_body
#         with open("output_STOL.json", "w") as json_file:
#             json.dump(error_msg, json_file)
#         sys.exit()
